# VSCode Extensions

## Junyi Academy Frontend Snippets

We strongly recommend you download this [vscode extension](https://marketplace.visualstudio.com/items?itemName=junyiacademy.junyi-frontend-snippets) to get better development experience.
