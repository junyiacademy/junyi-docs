# IntegratedTable

## 前情提要
2020 年 11 月起 data team 將產生 IntegratedTable 的 ETL jobs 從 jenkins 搬移到 airflow 的工程並進行重構，從原先在 python 檔中嵌入 SQL 去 query BigQuery 的模式，改寫成 [BigQuery Scripting](https://cloud.google.com/bigquery/docs/reference/standard-sql/scripting) 並存成 [Stored Procedure](https://cloud.google.com/bigquery/docs/reference/standard-sql/data-definition-language#create_procedure) 以利進行 SQL code 的版控。

## 簡介
本文件會紀錄 IntegratedTable 中每個 table 的 schema。

### Info_Date
用來紀錄從 2012-10-12 至今每個日期的資訊，包括是否寒暑假、學期別、是否放假等。

| column name | type | description |  
| ---- | ---- | ---- | 
| date | DATE | 日期 | 
| year_week | STRING | 哪一年的哪一週，這邊用的是 isoweek 機制。 | 
| year | INT | 該日期的年 | 
| month | INT | 該日期的月 | 
| day | INT | 該日期的日 | 
| weekday | INT | 星期幾，週一為 1，週日為 7 | 
| week | STRING | 該週的週一日期（備註：週一為 DB 備份日） | 
| session_type | STRING | 學期類別，有四種可能：`semester1`、`semester2`、`summer_vacation`、`winter_vacation` | 
| session_year | INT | 學年度的西元年，寒假及第二學期會是算前一個年度。例如：2020-05-16 的 session_year 會是 2019。| 
| session_year_TW | INT | 學年度的民國年，寒假及第二學期會是算前一個年度。例如：2020-05-16 的 session_year 會是 108。| 
| session_year_type_TW | STRING | 學年度 + 學期類別。以 108 學年度為例，有四種可能： `108暑假`、`108上`、`108寒假`、`108下`。| 
| is_holiday | BOOL | 是否為假日（含六日、國定假日、寒暑假） | 
