# Google SDK
## datastore
In Python 2 Project, we use SDK [`google.appengine.ext.db`](https://cloud.google.com/appengine/docs/standard/python/refdocs/google.appengine.ext.db) to access Datastore.

In Python 3, we use SDK [`google.cloud.datastore`](https://googleapis.dev/python/datastore/latest/).

The Python 3 SDK `google.cloud.datastore` is a lightweight Datastore API Python wrapper, while the Python 2 SDK `google.appengine.ext.db` provide more features.

There's another Python 3 SDK [`google.cloud.ndb`](https://googleapis.dev/python/python-ndb/latest/) would be more similar to the Python 2 SDK `google.appengine.ext.db`.

# Policy
## Transaction
過往讀寫資料庫都不會抓 transaction，這個規格在此專案修正為 **所有寫入都要抓 transaction，讀取需要 strong consistancy 的資料也抓 transaction**，其餘參考業務邏輯

相關的業務邏輯：Topic (資料夾概念）為多版本的資料，只有 edit version 的 Topic 會變動，除此之外的版本（如 default，也就是目前網站線上的版本）都不會修改，所以 edit version 的 Topic 讀寫都要抓 transaction，而其他版本不允許寫入，讀取不必抓 transaction。

## API test coverage
* For every API, please include at least one happy path test without mocking data. (Read data from Datastore emulator)
* If your test changed the database, please revert it in teardown. (The database should be unchanged when all tests are passed)

## Docs
* Please write [OpenAPI](https://swagger.io/docs/specification/about/) doc for new API. We use [Swagger](https://swagger.io/) for doc.
