# Package 依賴關係規範

為了避免重蹈 junyiacademy 的覆轍，參考 Khan Academy 的經驗，我們建立了 package 之間依賴關係的規範：為每個 package 定義層級，**依賴關係必須從高階至低階，且同階層之間不能有依賴關係。**

下圖是 Python3 Content Service 目前每個 package 的依賴關係，`main.py` 所在層級為最高階，`api`, `api_client`, `cache` 所在為最低階。

![simple_all_package](img/simple_all_package.png)


# Content package 內的層級規範

1. **API 層** (`main.py`, `api_implement.py`)
    1. 操作 content 的外部介面與 `user_util` 等 package
    2. 不能操作 datastore
2. **Content 層** (下圖左下角的 `content.topic` 和 `content.factory`)
    1. 負責操作一或多種 content 以提供外部介面，或修飾 content 的欄位名稱、格式以符合 API 規格
    2. 提供 content factory
    3. 不能操作 datastore
    4. 不能操作 content 以外的行為，e.g. user
3. **Content.internal** (下圖中 `content.topic` 和 `content.factory` 以外的 module)
    1. 負責定義單一 content, 操作 datastore

![content_packages](img/content_packages.png)