# Project Contribution 開發流程說明

## How to take an issue? 

1. Issues labeled with `good first issue` is a good place to start to contribute to Junyi Academy. 
Otherwise, you can browse project overview issues listed in the `README` of each repo.
2. Contact the project owner directly or leave a comment in the issue you want to take.
3. Project owner will assign you the issue if it's good to go.

## How to get my MR reviewed?

1. Assign the project owner and label your MR with `r?` label.
   - `r?` means your MR is ready for review
   - You can find project owner's name in the issue description
2. After few day, you will receive either `r+` or `r-` by reviewer
    - `r+` means your MR is approved and you can click merge button to merge it by yourself
    - `r-` means your MR need to be fixed or reviewer has some questions waiting for your reply, please fix MR or reply the question then label `r?` again

## Can I help code review?

1. Welcome to let the project owner know you are willing to do it.
1. Project owner will assign the MR to you once an appropriate MR waiting for a reviewer.

## Can I open issue?

1. Welcome to open issues and leave a comment to the corresponding meta issue.
1. Project owner will link your issues with meta issue.




