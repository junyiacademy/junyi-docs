# Project structure: How to find the issue which interest you ?  認識專案結構：如何找到你有興趣的任務？

Projects in Junyi Academy will be managed by three-layer issue structure and topic labels.

```
project overview issue ─┬─> meta issue ─┬─> issue
                        │   (topic1)    ├─> issue
                        │   (topic2)    ├─> issue
                        │               └─> issue
                        │
                        └─> meta issue ─┬─> issue
                            (topic1)    ├─> issue
                            (topic3)    ├─> issue
                                        └─> issue
```
ex:

- In `project overview issue`, you can see **what's the purpose of this project** and how can it benefit users in the big picture.

- A `meta issue` is a big task. Every meta issue has lots of `issues` (e.g: `Build login page` is a complicate task, so it will seperated by `create input component`, `create button component` and `connect API` ..... multiple child issues)

- An `issue` will describe the detail spec, **Just find the issue excitng you and start coding !** 


# 1. Project Overview Issue


## What is Project Overview Issue?

- Project Overview Issue is a kind of issue which you can see what's the purpose of this project and how can it benefit users in a big picture.


## Management Guideline

- A Project overview issue should be listed in README.md once created.
- A Project overview issue should be set blocked by it's related Meta issues
- Title must be prefixed with [Project Overview]. ex: `[Project Overview] Your title...`

### Template

A Project overview issue must follow this template and looks like below:

```markdown
<!--- Project overview issue must have [Project Overview] prefix in the title -->

# Summary

# Relevant Information

# List of Topics

<!--- List all topics and corresponding meta issues -->

## Topic Name

### Topic Board - [link](https://...)

### Meta Issues

+ #<meta_issue_number> - title of the meta issue without [Meta] prefix
+ #<meta_issue_number> - title of the meta issue without [Meta] prefix
+ #<meta_issue_number> - title of the meta issue without [Meta] prefix

```


# 2. Meta Issue


## What is Meta Issue?

- `Meta Issue` is a technical goal, it will be separated to lots of small issues.
- e.x: `Build login page` is a meta issue. This goal is too big and complicate, so it will be separated into `create input component`, `create button component` and `connect API` ..... multiple child issues

## Management Guideline

- A Project overview issue should be listed in README.md once created.
- A Project overview issue should be set blocked by it's related Meta issues
- Title must be prefixed with [Project Overview]. ex: `[Project Overview] Your title...`

### Template

A Meta issue must follow this template and look like below: 

```markdown
<!--- Meta issue must have [Meta] prefix in the title -->

# Summary

# List of Workflows

<!--- What should we do if we want to accomplish this meta issue? -->

- [ ] ...
- [ ] ...
```


# 3. Issue


## What's Issue ?

- An `issue` will describe the detail spec, **you can assign yourself with any Issue intresting you and just start coding !**


## Management Guideline

- An Issue Title must be prefixed with [Project Overview].  ex: `[Project Overview] Your title...`
- Every Issue must be labeled with the topic labels following it's meta issue.

- An Issue must be set to block meta issue.


### Template

A Issue description must follow this template and looks like below: 

```markdown
<!--- Provide a general summary of the issue in the Title above -->

# Detailed Description

<!--- Provide a detailed description of the change or addition you are proposing -->
<!--- Provide SPEC here -->

# Context

<!--- Why is this change important to you? How would you use it? -->
<!--- How can it benefit other users? -->

# Definition of Done

<!--- Make it short and clear -->

- [ ] ...

# Possible Implementation

<!--- Not obligatory, but suggest an idea for implementing addition or change -->

# Useful Resources

<!--- Provide the links here that may help -->

```

# 4. Topic (Label)

## What's Topic ?

- Topic is a kind of `label` which let developer know what's the purpose or the type of an issue
- e.x: Meta Issue `build login page` may have `Topic: on-boarding` and `Topic: Membership management` label at the same time

## Management Guideline

1. A Topic label must be created with `Topic: Example Topic` format [here](https://gitlab.com/groups/junyiacademy/-/labels). 
1. You must create a topic board [here](https://gitlab.com/groups/junyiacademy/-/boards) once a topic label was created.
1. Add this topic to **List of Topics** section of project overview issues.










